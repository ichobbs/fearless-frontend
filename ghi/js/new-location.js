window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
        const states_dict = await response.json();
        const states = states_dict.states;

        const sTag = document.getElementById("state");

        for (let index in states) {
            const option = document.createElement('option');
            const pair = states[index]
            option.value = Object.values(pair);
            option.innerHTML = Object.keys(pair);
            sTag.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async(event) => {
        event.preventDefault();
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });

});
