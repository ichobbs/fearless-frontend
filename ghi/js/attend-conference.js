window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const list = selectTag.classList;

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }

    const idElement = document.getElementById('loading-conference-spinner')
    const loadList = idElement.classList;
    loadList.add("d-none")
    list.remove("d-none")

    // This is the process for submitting a form
    // Remember in the HTML the elements must have 'name="nameUsedInJsonBody"'
    // Get the form element from the html by using getElementById
    const attendee = document.getElementById('create-attendee-form');
    // Create an event listener for the submit action which
    // will asyncrionously wait for a response (the form submission)
    window.addEventListener('submit', async (event) => {
        // this prevents the default form submission behavior, which is to reload the entire page
        event.preventDefault();
        // this creates a form data object from the form that we referenced in the getElementById
        const formData = new FormData(attendee)
        // a formData object is XML by default (for some reason) so we need to jsonify it
        const json = JSON.stringify(Object.fromEntries(formData))
        // the fetch congif should mirror some of the options you see in insomnia.
        // in this case, we are using the post method, delivering json, and require these headers
        console.log(json)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }

        // the url we need to go to
        const url = 'http://localhost:8001/api/attendees/'

        // we're going to fetch data, a post request per out fetch config, to the url,
        // and await a response from it
        const response = await fetch(url, fetchConfig);
        console.log(response)
        // if the reponse was good, we're going to reset the form
        // and we're going to get the response back
        if (response.ok) {
            attendee.reset();
            const addedAttendee = await response.json();
        }
    });

  });

