function createCard(name, description, pictureUrl, date, location) {
    return `
    <div class="my-3">
        <div class="card">
            <div class="shadow-lg p-3 mb-5 bg-body rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-subtitle mb-2 text-muted">${location}</p>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                <p class="card-text">${date}</p>
                </div>
            </div>
        </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const alertList = document.querySelectorAll('.container')
        const alerts =  [].slice.call(alertList).map(function (element) {
          return new bootstrap.Alert(element)
        })
      } else {
        const data = await response.json();
        let count = 1;
        for (let conference of data.conferences) {

          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const startString = details.conference.starts;
            const startDate = new Date(startString);
            const startDay = startDate.getDate();
            const startMonth = startDate.getMonth();
            const startYear = startDate.getFullYear();

            const endString = details.conference.ends;
            const endDate = new Date(endString);
            const endDay = endDate.getDate();
            const endMonth = endDate.getMonth();
            const endYear = endDate.getFullYear();

            const date = `${startMonth}/${startDay}/${startYear} - ${endMonth}/${endDay}/${endYear}`;

            const locationName = details.conference.location.name;

            const html = createCard(title, description, pictureUrl, date, locationName);

            let myString = `#col${count}`
            let column = document.querySelector(myString);
            column.innerHTML += html;
          }
          if (count < 3) {
            count++;
          } else {
            count = 1;
          }

        }

      }
    } catch (e) {
        const alertList = document.querySelectorAll('.container')
        const alerts =  [].slice.call(alertList).map(function (element) {
          return new bootstrap.Alert(element)
        })
    }

  });
